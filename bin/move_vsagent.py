#!../../../../bin/python
import os
import re
import sys
import tarfile

SPLUNK_DIR = str(os.getcwd())
PYTHON_DIR = str(os.environ["_"])

PLATFORM = str(sys.platform)

if PLATFORM.startswith("win"):
    print "win"
else:
    src = re.sub('(\/bin\/splunk)$',r'/etc/apps/wg_all_vs_agent_inputs/install/wg_all_vs_agent_inputs.tar.gz',PYTHON_DIR)
    dest = re.sub('(\/apps\/.+)','/deployment-apps/',src)
    tar = tarfile.open(src)
    tar.extractall(dest)
