#!../../../../bin/python

import os
import sys
import re

SPLUNK_DIR = str(os.getcwd())
PYTHON_DIR = str(os.environ["_"])
PLATFORM = str(sys.platform)
# /opt/splunk/bin/splunk
new_class = """
# whitelist matches all clients.
[global]
[serverClass:vsa_all_clients]
whitelist.0=*
[serverClass:vsa_all_clients:app:wg_all_vs_agent_inputs]
restartSplunkd = true
"""

if PLATFORM.startswith("win"):
    print PLATFORM
else:
    sc_path = re.sub('(\/bin\/splunk)$',r'/etc/system/local/serverclass.conf',PYTHON_DIR)
    sc_file = open(sc_path,"w+")
    sc_file.write(new_class)
    sc_file.close()
